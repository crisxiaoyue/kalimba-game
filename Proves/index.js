var myVar;
var song1 = [2,3,4,6,2,1,4,2,6,1,6,7,1,0,6,5,2,3,6,1,4,0,3,6,1,0,2];
var colors = ['red', 'blue', 'yellow', 'brown', 'green', 'purple', 'pink', 'organe'];
var notesAtocar = [0,0,0,0,0,0,0,0];
var counter = 0;
var puntuacio = 0;
var elemPuntuacio = document.getElementById("puntuacio");
var laminaMusica = {
    "do_greu" : ".\\notes\\1.mp3",
    "re" : ".\\notes\\2.mp3",
    "mi" : ".\\notes\\3.mp3",
    "fa" : ".\\notes\\4.mp3",
    "sol" : ".\\notes\\5.mp3",
    "la" : ".\\notes\\6.mp3",
    "si" : ".\\notes\\7.mp3",
    "do_agut" : ".\\notes\\8.mp3"
}

var teclaMusica = {
    /**MAJUSCULES */
    81 : ".\\notes\\1.mp3",
    87 : ".\\notes\\2.mp3",
    69 : ".\\notes\\3.mp3",
    82 : ".\\notes\\4.mp3",
    84 : ".\\notes\\5.mp3",
    89 : ".\\notes\\6.mp3",
    85 : ".\\notes\\7.mp3",
    73 : ".\\notes\\8.mp3",
    /**MINUSCULES */
    113 : ".\\notes\\1.mp3",
    119 : ".\\notes\\2.mp3",
    101 : ".\\notes\\3.mp3",
    114 : ".\\notes\\4.mp3",
    116 : ".\\notes\\5.mp3",
    121 : ".\\notes\\6.mp3",
    117 : ".\\notes\\7.mp3",
    105 : ".\\notes\\8.mp3"
}

/**VERSIÓ NOVA - FA SONAR LA NOTA UN COP APRETADA AMB EL RATOLÍ. 
 * NO CONTROLA PUNTS */
function nota(str){
    var nota = str+"";
    var path = laminaMusica[nota];
    var snd = new Audio(path);
    snd.play();

    switch(nota){
        case 'do_greu':
            if( notesAtocar[0] == 1){
                puntuacio +=10;
                notesAtocar[0] = 0;
            }
            break;
        case 're':
            if( notesAtocar[1] == 1){
                puntuacio +=10;
                notesAtocar[1] = 0;
            }
            break;
        case 'mi':
            if( notesAtocar[2] == 1){
                puntuacio +=10;
                notesAtocar[2] = 0;;
            }
            break;
        case 'fa':
            if( notesAtocar[3] == 1){
                puntuacio +=10;
                notesAtocar[3] = 0;;
            }
            break;
        case 'sol':
            if( notesAtocar[4] == 1){
                puntuacio +=10;
                notesAtocar[4] = 0;;
            }
            break;
        case 'la':
            if( notesAtocar[5] == 1){
                puntuacio +=10;
                notesAtocar[5] = 0;;
            }
            break;
        case 'si':
            if( notesAtocar[6] == 1){
                puntuacio +=10;
                notesAtocar[6] = 0;;
            }
            break;
        case 'do_agut':
            if( notesAtocar[7] == 1){
                puntuacio +=10;
                notesAtocar[7] = 0;;
            }
            break;
        }

        var elemPuntuacio = document.getElementById("puntuacio");
        elemPuntuacio.innerText = puntuacio;

}



function tecla(event){
    var x = event.keyCode;
    var path = teclaMusica[x];
    

    var snd = new Audio(path);
    snd.play();
}


function start(){
    myVar = setInterval(startSound, 1000);
}

function startSound(){
    var i = 0;
    for(i; i < notesAtocar.length; i++){
        notesAtocar[i] = 0;
    }
    var nota = song1[counter];
    notesAtocar[nota] = 1;
    var elem = document.getElementById("cuadrat");
    elem.style.backgroundColor = colors[nota];  
    counter++;
    if(counter == song1.length){
        clearInterval(myVar);
    }

}
