var myVar;
var song1 = [0,1,2,4,4,5,4,2,0,0,1,2,2,1,0,1];
var song2 = [3,4,3,3,3,0,0,0,5,3,3,4,3,3,3,0,0,1,2,3];
var song3 = [2,3,4,2,0,3,2,1,1,0,1,2,0,2,3,4,2,0,3,2,1,0,1,2,0];
var notesAtocar = [0,0,0,0,0,0,0,0];
var counter = 0;
var puntuacio = 0;
var autoplay;
var jocIniciat = false;
var elemPuntuacio = document.getElementById("puntuacio");
var laminaMusica = {
    "0" : ".\\notes\\1.mp3",
    "1" : ".\\notes\\2.mp3",
    "2" : ".\\notes\\3.mp3",
    "3" : ".\\notes\\4.mp3",
    "4" : ".\\notes\\5.mp3",
    "5" : ".\\notes\\6.mp3",
    "6" : ".\\notes\\7.mp3",
    "7" : ".\\notes\\8.mp3",
    "8" : ".\\notes\\error.mp3"
}

var teclaMusica = {
    /**MAJUSCULES */
    81 : ".\\notes\\1.mp3",
    87 : ".\\notes\\2.mp3",
    69 : ".\\notes\\3.mp3",
    82 : ".\\notes\\4.mp3",
    84 : ".\\notes\\5.mp3",
    89 : ".\\notes\\6.mp3",
    85 : ".\\notes\\7.mp3",
    73 : ".\\notes\\8.mp3",
    /**MINUSCULES */
    113 : ".\\notes\\1.mp3",
    119 : ".\\notes\\2.mp3",
    101 : ".\\notes\\3.mp3",
    114 : ".\\notes\\4.mp3",
    116 : ".\\notes\\5.mp3",
    121 : ".\\notes\\6.mp3",
    117 : ".\\notes\\7.mp3",
    105 : ".\\notes\\8.mp3"
}

var teclaNota = {
    /**MAJUSCULES */
    81 : 0,
    87 : 1,
    69 : 2,
    82 : 3,
    84 : 4,
    89 : 5,
    85 : 6,
    73 : 7,
    /**MINUSCULES */
    113 : 0,
    119 : 1,
    101 : 2,
    114 : 3,
    116 : 4,
    121 : 5,
    117 : 6,
    105 : 7
}

function obreMenu() {
    document.getElementById("menu").style.width = "18%";
}

function tancaMenu() {
    document.getElementById("menu").style.width = "0";
}