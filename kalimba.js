/**VERSIÓ NOVA - FA SONAR LA NOTA UN COP APRETADA AMB EL RATOLÍ. 
 * NO CONTROLA PUNTS */
function nota(nota) {
    var path = laminaMusica[nota];
    var snd = new Audio(path);

    if (notesAtocar[nota] == 1) {
        if(!autoplay){
            puntuacio += 10;
            notesAtocar[nota] = 0;
        }
        //Marcar nota correcta
        var element = document.getElementById(nota);
        element.classList.add('lamina');
        
        var e = document.getElementById("estrella");
        e.classList.add("star-1");
    } else {
        if (jocIniciat) {
            path = laminaMusica[8];
            snd = new Audio(path);
            //Marcar nota incorrecta
            var element = document.getElementById(nota);
            element.classList.add("notaIncorrecta");
        }
    }

    snd.play();
}

function apretarLamina(str) {
    if(!autoplay){
        try {
            var nota = str + "";
            var lamina = document.getElementById(nota);
    
            lamina.classList.add("apretada");
        } catch (error) {/**Evitar que surti error quan es toca una lletra que no és. */ }
    }
    
}

function alliberarLamina(str) {
    if(!autoplay){
        try {
            var nota = str + "";
            var lamina = document.getElementById(nota);
    
            lamina.classList.remove("apretada");
        } catch (error) {/**Evitar que surti error quan es toca una lletra que no és. */ }
    } 
}

function tecla(event) {
    if(!autoplay){
    // Manca fer l'animació quan utilitzes el teclat
    // by Joan Rodríguez
    var x = event.keyCode;
    var path = teclaMusica[x];

    var nota = teclaNota[event.keyCode]

    apretarLamina(nota);

    var snd = new Audio(path);

    if (notesAtocar[nota] == 1) {
        try {
            puntuacio += 10;
            notesAtocar[nota] = 0;

            //Marcar nota correcta
            var element = document.getElementById(nota);
            element.classList.add('lamina');
            var e = document.getElementById("estrella");
            e.classList.add("star-1");
        } catch (error) { }
    } else {
        if (jocIniciat) {
            try {
                if (nota != null) {
                    path = laminaMusica[8];
                    snd = new Audio(path);

                    //Marcar nota incorrecta
                    var element = document.getElementById(nota);
                    element.classList.add("notaIncorrecta");
                }

            } catch (error) { }
        }
    }
    snd.play();
    }
    
}

function alliberarTecla(event) {
    alliberarLamina(teclaNota[event.keyCode]);

}

function start() {
    /**Comprovar si hi ha l'autoplay */

    //Desactivar el boto Start un cop s'ha iniciat el joc.
    document.getElementById("button").style.pointerEvents = 'none';
    document.getElementById("autoplay").style.pointerEvents = 'none';
    document.getElementById("cansons").style.pointerEvents = 'none';
    //var checkbox = document.getElementById("checkBox");
    if(document.getElementById("checkBox").checked == true){
        autoplay = true;
    } else{
        autoplay = false;
    }
    if(autoplay){
        for (var i = 0; i < 8; i++) {
            var element = document.getElementById(i);
            element.style.pointerEvents = 'none';
        }
    }
    jocIniciat = true;
    //Iniciar joc.
    myVar = setInterval(startSound, 1000);
}

function finish() {
    /**PERPARAR EL JOC UN ALTRE COP. */
    //Activar boto start
    document.getElementById("button").style.pointerEvents = 'auto';
    document.getElementById("autoplay").style.pointerEvents = 'auto';
    document.getElementById("cansons").style.pointerEvents = 'auto';
    counter = 0;
    jocIniciat = false;
    autoplay = false;
    if(!autoplay){
        for (var i = 0; i < 8; i++) {
            var element = document.getElementById(i);
            element.style.pointerEvents = 'auto';
        }
    }

    //Assegurar que cap lamina tingui classes.
    resetejarLamines();
}

/**Funció que es va executant cada segons fins arribar al final de la canço.
 * Primer de tot buida l'array amb la nota que s'ha de tocar al moment.
 * Tot seguit escrtiu a la posició de la nota que s'ha de tocar un 1.
 * canvia el color 
 */
function startSound() {

    if (document.getElementById('song1').checked) {
        var nota1 = song1[counter];
    } else if (document.getElementById('song2').checked) {
        var nota1 = song2[counter];
    } else if (document.getElementById('song3').checked) {
        var nota1 = song3[counter];
    }

    //Array que controla quina nota s'ha de tocar cada moment.
    for (var i = 0; i < notesAtocar.length; i++) {
        notesAtocar[i] = 0;
    }
    notesAtocar[nota1] = 1;

    if(autoplay){
        nota(nota1);
    }
    

    resetejarLamines();

    for (var i = 0; i < 8; i++) {
        if (i == nota1) {
            //Marcar nota a tocar
            var element = document.getElementById(i);
            element.classList.add("tocarNota");
            element.classList.remove("notaIncorrecta");

        } else {
            //Tornar nota al color original
            var element = document.getElementById(i);
            element.classList.remove("tocarNota");
            element.classList.remove("notaIncorrecta");
        }
    }
    var element = document.getElementById("estrella");
    element.classList.remove("star-1");



    counter++;

    /*
        SETTIMEOUT
        sense parametres: setTimeout(funcio, temps);
        amb parametres: setTimeout(funcio.bind(null, param1), temps)
    */

    if (document.getElementById('song1').checked) {
        if (counter == song1.length) {
            clearInterval(myVar);
            setTimeout(finish, 5000);
        }
    } else if (document.getElementById('song2').checked) {
        if (counter == song2.length) {
            clearInterval(myVar);
            setTimeout(finish, 5000);
        }
    } else if (document.getElementById('song3').checked) {
        if (counter == song3.length) {
            clearInterval(myVar);
            setTimeout(finish, 5000);
        }
    }

    elemPuntuacio.innerText = "Puntuació: " + puntuacio;
}

function resetejarLamines() {
    for (var i = 0; i < 8; i++) {
        var element = document.getElementById(i);
        element.classList.remove("tocarNota");
        element.classList.remove("notaIncorrecta");
        element.classList.remove("lamina");
        
    }
}